from flask import abort
from flask import current_app, jsonify
from flask import g
from flask import render_template
from flask import request
from flask import session

from info import constants, db
from info.models import User, News, Comment, CommentLike
from info.utils.common import user_login_data
from info.utils.response_code import RET
from . import news_blu


@news_blu.route('/followed_user', methods=["POST"])
@user_login_data
def followed_user():
    """关注/取消关注用户"""
    if not g.user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    user_id = request.json.get("user_id")
    action = request.json.get("action")

    if not all([user_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    if action not in ("follow", "unfollow"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 查询到关注的用户信息
    try:
        target_user = User.query.get(user_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据库失败")

    if not target_user:
        return jsonify(errno=RET.NODATA, errmsg="未查询到用户数据")

    if target_user.id != g.user.id:
        # 根据不同操作做不同逻辑
        if action == "follow":
            if target_user.followers.filter(User.id == g.user.id).count() > 0:
                return jsonify(errno=RET.DATAEXIST, errmsg="当前已关注")
            target_user.followers.append(g.user)
        else:
            if target_user.followers.filter(User.id == g.user.id).count() > 0:
                target_user.followers.remove(g.user)

        # 保存到数据库
        try:
            db.session.commit()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.DBERR, errmsg="数据保存错误")
    else:
        return jsonify(errno=RET.PARAMERR, errmsg="用户不能自己关注自己")

    return jsonify(errno=RET.OK, errmsg="操作成功")


@news_blu.route("/<int:news_id>")
@user_login_data
def news_detail(news_id):
    """
    返回新闻详情页面
    :param news_id:
    :return:
    """
    # 1.详情页右上角用户登录信息显示
    # 1.1从session中获取当前用户登录信息，有则返回当前登录用户信息，无则为None
    # user_id = session.get("user_id", None)
    # user = None
    # if user_id:
    #     try:
    #         user = User.query.get(user_id)
    #     except Exception as e:
    #         current_app.logger.error(e)
    user = g.user
    # 2.显示新闻排行信息
    news_list = []
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    news_dict_li = []
    for news in news_list:
        news_dict_li.append(news.to_basic_dict())

    news = None
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)

    if not news:
        abort(404)

    news.clicks += 1
    is_collected = False

    if user:
        if news in user.collection_news:
            is_collected = True

    comments_list = None
    try:
        # comments_list = Comment.query.filter(Comment.news_id == news_id).order_by(Comment.create_time.desc()).all()
        comments_list = Comment.query.filter(Comment.news_id == news_id).order_by(Comment.create_time.desc()).all()
    except Exception as e:
        current_app.logger.error(e)

    comment_like_ids = []
    if g.user:
        # 如果当前用户已登录
        try:
            comment_ids = [comment.id for comment in comments_list]
            if len(comment_ids) > 0:
                # 取到当前用户在当前新闻的所有评论点赞的记录
                comment_likes = CommentLike.query.filter(CommentLike.comment_id.in_(comment_ids),
                                                         CommentLike.user_id == user.id).all()
                # 取出记录中所有的评论id
                comment_like_ids = [comment_like.comment_id for comment_like in comment_likes]
        except Exception as e:
            current_app.logger.error(e)

    comments_dict_list = []
    for comments in comments_list if comments_list else []:
        comments_dict = comments.to_dict()
        # 添加点赞状态标记属性去控制前端页面点赞状态的显示
        comments_dict["is_like"] = False

        if comments.id in comment_like_ids:
            comments_dict["is_like"] = True
        comments_dict_list.append(comments_dict)
    is_followed = False
    # if 当前新闻有作者，并且 当前登录用户已关注过这个用户
    if news.user and user:
        # if user 是否关注过 news.user
        if news.user in user.followed:
            is_followed = True

    data = {
        "user": user.to_dict() if user else None,
        "news_dict_li": news_dict_li,
        "news": news.to_dict(),
        "is_collected": is_collected,
        "comments": comments_dict_list,
        "is_followed": is_followed
    }
    return render_template("news/detail.html", data=data)


@news_blu.route("/news_collect", methods=["POST"])
@user_login_data
def news_collect():
    """
    收藏、取消收藏
    :return:
    """
    user = g.user
    # 1.判断当前用户登录状态
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")
    # 2.获取参数
    news_id = request.json.get("news_id", None)
    action = request.json.get("action", None)
    # 3.判断参数
    if not all([news_id, action]):
        return jsonify(errno=RET.DBERR, errmsg="参数错误")

    try:
        news_id = int(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="参数错误")

    if action not in ['collect', 'cancel_collect']:
        return jsonify(errno=RET.DBERR, errmsg="参数错误")
    # 4.查询数据库
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    if not news:
        return jsonify(errno=RET.DATAERR, errmsg="新闻数据不存在")
    # 5.收藏/取消收藏
    if action == "cancel_collect":
        if news in user.collection_news:
            user.collection_news.remove(news)
    else:
        if news not in user.collection_news:
            user.collection_news.append(news)

    return jsonify(errno=RET.OK, errmsg="操作成功")


@news_blu.route("/news_comment", methods=["POST"])
@user_login_data
def news_comment():
    user = g.user
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")
    # 1. 取到请求参数
    news_id = request.json.get("news_id", None)
    comment_content = request.json.get("comment", None)
    parent_id = request.json.get("parent_id", None)
    # 2. 判断参数
    if not all([news_id, comment_content]):
        return jsonify(errno=RET.DBERR, errmsg="参数错误")
    try:
        news_id = int(news_id)
        if parent_id:
            parent_id = int(parent_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="参数错误")

    # 查询新闻，并判断新闻是否存在
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    if not news:
        return jsonify(errno=RET.DATAERR, errmsg="新闻数据不存在")
    if parent_id:
        comment = Comment.query.get(parent_id)
        if comment.user_id == user.id:
            return jsonify(errno=RET.DBERR, errmsg="抱歉，您不能回复自己的评论,请取消！")

    # 3. 初始化一个评论模型，并且赋值
    comment = Comment()
    comment.news_id = news_id
    comment.user_id = user.id
    comment.content = comment_content
    if parent_id:
        comment.parent_id = parent_id
    # 添加到数据库
    # 为什么要自己去commit()?，因为在return的时候需要用到 comment 的 id
    try:
        db.session.add(comment)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据添加失败")

    return jsonify(errno=RET.OK, errmsg="OK", data=comment.to_dict())


@news_blu.route("/comment_like", methods=["POST"])
@user_login_data
def comment_like():
    # 1.判断用户是否登录
    user = g.user
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")
    # 2.获取参数
    comment_id = request.json.get("comment_id", None)
    action = request.json.get("action", None)

    # 4. 判断参数
    if not all([comment_id, action]):
        return jsonify(errno=RET.DBERR, errmsg="参数错误")
    try:
        comment_id = int(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="参数错误")

    if action not in ['add', 'remove']:
        return jsonify(errno=RET.DBERR, errmsg="参数错误")

    # 5.查询评论，并判断评论是否存在
    try:
        comment = Comment.query.get(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    if not comment:
        return jsonify(errno=RET.DATAERR, errmsg="评论数据不存在")

    # 6.点赞或者取消点赞
    if action == "add":
        comment_like_model = CommentLike.query.filter(CommentLike.user_id == user.id,
                                                      CommentLike.comment_id == comment.id).first()
        if not comment_like_model:
            # 点赞评论
            comment_like_model = CommentLike()
            comment_like_model.user_id = user.id
            comment_like_model.comment_id = comment.id
            db.session.add(comment_like_model)
            comment.like_count += 1
    else:
        comment_like_model = CommentLike.query.filter(CommentLike.comment_id == comment_id,
                                                      CommentLike.user_id == user.id).first()
        if comment_like_model:
            db.session.delete(comment_like_model)
            comment.like_count -= 1

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="操作失败")
    return jsonify(errno=RET.OK, errmsg="操作成功")
