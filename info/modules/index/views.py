from flask import current_app, jsonify
from flask import g
from flask import render_template
from flask import request
from flask import session
from info import constants
from info.models import User, News, Category
from info.utils.common import user_login_data
from info.utils.response_code import RET
from . import index_blu


@index_blu.route("/")
@user_login_data
def index():
    # user_id = session.get("user_id", None)
    # user = None
    # if user_id:
    #     try:
    #         user = User.query.get(user_id)
    #     except Exception as e:
    #         current_app.logger.error(e)
    user = g.user
    # 首页新闻点击排行逻辑
    # 1.去数据库中查数据
    news_list = []
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    news_dict_li = []
    for news in news_list:
        news_dict_li.append(news.to_basic_dict())
    try:
        categories = Category.query.all()

    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")
    category_dict_list = []
    for category in categories:
        category_dict_list.append(category.to_dict())

    data = {
        "user": user.to_dict() if user else None,
        "news_dict_li": news_dict_li,
        "category_dict_list": category_dict_list
    }

    return render_template("news/index.html", data=data)


@index_blu.route("/favicon.ico")
def favicon():
    return current_app.send_static_file("news/favicon.ico")


@index_blu.route("/news_list")
def news_list():
    """
    加载首页左侧新闻列表
    :return:
    """
    # 1.获取参数
    cid = request.args.get("cid", "1")
    page = request.args.get("page", "1")
    per_page = request.args.get("per_page", "10")

    # 2.校验参数
    try:
        cid = int(cid)
        page = int(page)
        per_page = int(per_page)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 3.查询数据
    filters = [News.status == 0]
    if cid != 1:
        filters.append(News.category_id == cid)

    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page,
                                                                                          per_page,
                                                                                          False)
        #     # 取到当前页数据
        news_model_list = paginate.items
        total_pages = paginate.pages
        current_page = paginate.page

    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")

    # # 将模型对象转换为字典列表
    news_dict_list = []
    for news in news_model_list:
        news_dict_list.append(news.to_basic_dict())

    data = {
        "total_pages": total_pages,
        "current_page": current_page,
        "news_dict_list": news_dict_list
    }

    return jsonify(errno=RET.OK, errmsg="OK", data=data)
