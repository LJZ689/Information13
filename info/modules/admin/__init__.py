from flask import Blueprint
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

admin_blu = Blueprint("admin", __name__)

from . import views


@admin_blu.before_request
def before_request():
    # 判断如果不是登录页面的请求
    if request.url.endswith(url_for("admin.admin_login")):
        user_id = session.get("user_id", "")
        is_admin = session.get("is_admin", False)
        if user_id != "" and not is_admin:
            return redirect('/')
    else:
        is_admin = session.get("is_admin", False)
        if not is_admin:
            return redirect('/')
