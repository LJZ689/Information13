# -*- coding: utf-8 -*-
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from info import create_app, db, models
from info.models import User

app = create_app("development")
# 集成flask-script
manager = Manager(app)
# 设置app db 的关联
Migrate(app, db)
# 将迁移命令添加到manager中
manager.add_command("db", MigrateCommand)


@manager.option("-n", "-name", dest="name")
@manager.option("-p", "-password", dest="password")
@manager.option("-m", "-mobile", dest="mobile")
def createsuperuser(name, password, mobile):
    if not all([name, password]):
        print("参数不足")

    user = User()
    user.is_admin = True
    user.nick_name = name
    user.mobile = mobile
    user.password = password

    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        db.session.roolback()
        print(e)
    print("添加成功")


if __name__ == '__main__':
    manager.run()
